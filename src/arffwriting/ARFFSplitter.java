package arffwriting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

public class ARFFSplitter {
	public static void main(String[] args) throws IOException {
		File arffFile = new File("so4.arff");
		File output1 = new File("train.arff");
		File output2 = new File("test.arff");
		FileWriter fw1 = new FileWriter(output1);
		FileWriter fw2 = new FileWriter(output2);
		List<String> lines = Files.lines(arffFile.toPath()).collect(Collectors.toList());
		boolean b = true;
		int cutOff = 0;
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i) + "\n";
			if (b) {
				fw1.append(line);
				fw2.append(line);
			}
			else if (i < cutOff) {
				fw1.append(line);
			}
			else {
				fw2.append(line);
			}
			if (line.contains("@DATA")) {
				b = false;
				cutOff = i + (int) (0.8*(lines.size() - i));
			}
		}
		fw1.close();
		fw2.close();
	}
}
