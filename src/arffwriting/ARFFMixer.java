package arffwriting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ARFFMixer {
	
	private static Set<String> ids = null;
	public static void main(String[] args) throws IOException {
		File outFile = new File("so4.arff");
		readIDS("stats2.csv");
		File[] inFiles = new File[] { new File("FullData.csv"),
					new File("pos-features1")
					new File("pos-features2"),
					new File("pos-features3"),
					new File("pos-features4"),
					new File("pos-features5")
		};
		FileWriter fw = new FileWriter(outFile);
		writeHeader(inFiles, fw);
		writeBodies(inFiles, fw);
		fw.close();
	}

	private static void readIDS(String string) throws IOException {
		ids = new HashSet<String>();
		File file = new File(string);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			String[] split = line.split(",");
			String id = split[0];
			ids.add(id);
		}
		br.close();
	}

	private static void writeBodies(File[] inFiles, FileWriter fw) throws IOException {
		BufferedReader[] brs = new BufferedReader[inFiles.length];
		for (int i = 0 ; i < inFiles.length; i++) {
			brs[i] = new BufferedReader(new FileReader(inFiles[i]));
			brs[i].readLine();
		}
		OUTER:
		while (true) {
			String[] lines = new String[brs.length];
			String id = null;
			lines[0] = brs[0].readLine();
			if (lines[0] == null) break;
			id = lines[0].substring(0, lines[0].indexOf(","));
			if (ids != null && !ids.contains(id)) {
				continue;
			}
			for (int i = 1; i < brs.length; i++) {
				BufferedReader br = brs[i];
				lines[i] = br.readLine();
				if (lines[i] == null) {
					break OUTER;
				}
			}
			for (int i = 0; i < lines.length; i++) {
				if (i == 0) {
					String toPrint = lines[i].substring(lines[i].indexOf(",") + 1);
					fw.append(toPrint);
				} else {
					fw.append(lines[i]);
				}
				if (i < lines.length - 1) fw.append(",");
				else fw.append("\n");
			}
		}
		for (int i = 0 ; i < brs.length; i++) {
			brs[i].close();
		}
	}

	private static void writeHeader(File[] inFiles, FileWriter fw)
			throws IOException, FileNotFoundException {
		fw.append(startHeader);
		int miscIDX = 0;
		for (File inFile : inFiles) {
			if (inFile.getName().contains("FullData")) continue;
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			for (String term : line.split(",")) {
				if (term.matches("\\p{Punct}+")) term = "" + miscIDX++;
				else term += "2";
				fw.append("@ATTRIBUTE " + term + "\tREAL\n");
			}
			br.close();
		}
		fw.append(endHeader);
	}
	
	
	private final static String startHeader = "@RELATION SOQuestions\n" +
			"\n" +
			"@ATTRIBUTE bodyLength	numeric\n" +
			"@ATTRIBUTE emailsCount	numeric\n" +
			"@ATTRIBUTE lowercasePerc	REAL\n" +
			"@ATTRIBUTE spacesCount	numeric\n" +
			"@ATTRIBUTE tagsCount	numeric\n" +
			"@ATTRIBUTE textSpeakCount	numeric\n" +
			"@ATTRIBUTE titleBodySimilarity	REAL\n" +
			"@ATTRIBUTE titleLength	numeric\n" +
			"@ATTRIBUTE capitalTitle	numeric\n" +
			"@ATTRIBUTE uppercasePerc	REAL\n" +
			"@ATTRIBUTE URLsCount	numeric\n" +
			"@ATTRIBUTE avgEntropy	REAL\n" +
			"@ATTRIBUTE automatedRI	REAL\n" +
			"@ATTRIBUTE colemanLiau	REAL\n" +
			"@ATTRIBUTE fleschGrade	REAL\n" +
			"@ATTRIBUTE fleschReading	REAL\n" +
			"@ATTRIBUTE gunningFog	REAL\n" +
			"@ATTRIBUTE locPercentage	REAL\n" +
			"@ATTRIBUTE metricEntropy	REAL\n" +
			"@ATTRIBUTE sentencesCount	numeric\n" +
			"@ATTRIBUTE smogGrade	REAL\n" +
			"@ATTRIBUTE wordsCount	numeric\n" +
			"@ATTRIBUTE lineLengthAVG	REAL\n" +
			"@ATTRIBUTE lineLenghtMax	numeric\n" +
			"@ATTRIBUTE identifiesrAVG	REAL\n" +
			"@ATTRIBUTE identifiersMax	numeric\n" +
			"@ATTRIBUTE idLengthAVG	REAL\n" +
			"@ATTRIBUTE idLengthMax	numeric\n" +
			"@ATTRIBUTE indentationAVG	REAL\n" +
			"@ATTRIBUTE indentationMax	numeric\n" +
			"@ATTRIBUTE keywordsAVG	REAL\n" +
			"@ATTRIBUTE keywordsMax	numeric\n" +
			"@ATTRIBUTE numbersAVG	REAL\n" +
			"@ATTRIBUTE numbersMax	numeric\n" +
			"@ATTRIBUTE comments	REAL\n" +
			"@ATTRIBUTE periods	REAL\n" +
			"@ATTRIBUTE commmas	REAL\n" +
			"@ATTRIBUTE spaces	REAL\n" +
			"@ATTRIBUTE parentheses	REAL\n" +
			"@ATTRIBUTE arithmOps	REAL\n" +
			"@ATTRIBUTE compOps	REAL\n" +
			"@ATTRIBUTE assignments	REAL\n" +
			"@ATTRIBUTE branches	REAL\n" +
			"@ATTRIBUTE loops	REAL\n" +
			"@ATTRIBUTE blanklines	REAL\n" +
			"@ATTRIBUTE singleCharsAVG	REAL\n" +
			"@ATTRIBUTE singleIDsAVG	REAL\n" +
			"@ATTRIBUTE EntrUnigram	REAL\n" +
			"@ATTRIBUTE EntrBigram	REAL\n" +
			"@ATTRIBUTE EntrTrigram REAL\n" +
			"@ATTRIBUTE class 	{Good,Bad}\n";
//			"@ATTRIBUTE answercount	numeric\n";
//			"@ATTRIBUTE score	numeric\n";
	
	private final static String endHeader = "\n@DATA\n";
}
