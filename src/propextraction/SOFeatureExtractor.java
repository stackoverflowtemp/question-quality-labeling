package propextraction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import propextraction.util.Patterns;
import propextraction.util.RawFile;
import propextraction.util.TextSpeak;

public class SOFeatureExtractor extends FeatureExtractor {

	public static void main(String[] args) {
		try {
			File fragmentsRoot = new File("../splitsnippets");
			File outputFile = new File("so-features.csv");
			FeatureExtractor extractor = new SOFeatureExtractor(fragmentsRoot);
			extractor.process(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public SOFeatureExtractor(File fragmentsRoot) {
		super(fragmentsRoot);
	}

	@Override
	protected void process(Integer id, FileWriter outputWriter) throws IOException {
		if (Math.random() < 0.025)
			System.out.println(id);
		RawFile rawFile = readRaw(getRawFile(id));
		String title = rawFile.getTitle();
		String tags = rawFile.getTags();
		String raw = rawFile.getContent();
		String text = readLines(getTextFile(id));
		int bodyLength = bodyLength(raw);
		int cleanedLength = bodyLength(text);
		int emailsCount = emailsCount(text);
		float lowercasePerc = lowercaseCount(text) / cleanedLength;
		int spacesCount = spacesCount(text);
		int tagsCount = tagsCount(tags);
		int textSpeakCount = textSpeakCount(text);
		float titleBodySimilarity = titleBodySimilarity(title, text);
		int titleLength = titleLength(title);
		int capitalTitle = capitalTitle(title);
		float uppercasePerc = uppercaseCount(text) / cleanedLength;
		int URLsCount = urlsCount(raw, text);
		outputWriter.append(id + "," + bodyLength + "," + emailsCount + "," + lowercasePerc + "," + spacesCount + "," + 
				tagsCount + "," + textSpeakCount + "," + titleBodySimilarity + "," + titleLength + "," +
				capitalTitle + "," + uppercasePerc + "," + URLsCount + "\n");
	}

	private int bodyLength(String content) {
		return content.length();
	}

	private int emailsCount(String content) {
		int emailCount = 0;
		Matcher m = Patterns.EMAIL_ADDRESS.matcher(content);
		while (m.find()) {
			emailCount++;
		}
		return emailCount;
	}

	private float lowercaseCount(String content) {
		int count = 0;
		for (char c : content.toCharArray()) {
			if (Character.isLowerCase(c)) count++;
		}
		return count;
	}

	private int spacesCount(String content) {
		int count = 0;
		for (char c : content.toCharArray()) {
			if (Character.isSpaceChar(c)) count++;
		}
		return count;
	}

	private int tagsCount(String tags) {
		int tagsCount = 0;
		Pattern tagMatcher = Pattern.compile("<[^>]+>");
		Matcher m = tagMatcher.matcher(tags);
		while (m.find()) {
			tagsCount++;
		}
		return tagsCount;
	}

	private int textSpeakCount(String content) {
		int count = 0;
		String lowerCase = content.toLowerCase();
		for (String word : lowerCase.split("\\s+")) {
			for (String x : TextSpeak.textSpeak) {
				if (x.equals(word)) count++;
			}
			for (String x : TextSpeak.emojis) {
				if (x.equals(word)) count++;
			}
		}
		return count;
	}

	private float titleBodySimilarity(String title, String content) {
		return cosineSimilarity(content, title);
	}

	private int titleLength(String title) {
		return title.length();
	}

	private int capitalTitle(String title) {
		if (Character.isUpperCase(title.trim().charAt(0))) return 1;
		return 0;
	}

	private float uppercaseCount(String content) {
		int count = 0;
		for (char c : content.toCharArray()) {
			if (Character.isUpperCase(c)) count++;
		}
		return count;
	}

	private int urlsCount(String raw, String cleaned) {
		int urlCount = 0;
		// First find all <a href> HTML tags in the raw text 
		Pattern aHrefPattern = Pattern.compile(Pattern.quote("</a>"));
		Matcher m1 = aHrefPattern.matcher(raw);
		while (m1.find()) {
			urlCount++;
		}
		// Now add all URLs in the question text
		Matcher m = Patterns.WEB_URL.matcher(cleaned);
		while (m.find()) {
			if (m.group().toLowerCase().matches("((http|ftp|https|rtsp)://|www\\.).*")) {
				urlCount++;
			}
		}
		return urlCount;
	}

	private float cosineSimilarity(String content, String titleLine) {
		Map<String, int[]> wordCounts = new HashMap<String, int[]>();
		Arrays.stream(titleLine.split("\\s+")).forEach(x -> {
			int[] count = wordCounts.get(x);
			if (count == null) {
				count = new int[2];
				wordCounts.put(x, count);
			}
			count[0]++;
		});
		Arrays.stream(content.split("\\s+")).forEach(x -> {
			int[] count = wordCounts.get(x);
			if (count == null) {
				count = new int[2];
				wordCounts.put(x, count);
			}
			count[1]++;
		});
		int[][] counts = new int[wordCounts.size()][];
		int index = 0;
		for (String key : wordCounts.keySet()) {
			counts[index++] = wordCounts.get(key);
		}
		double sumprod = 0.0;
		double div1 = 0.0, div2 = 0.0;
		for (int i = 0; i < counts.length; i++) {
			sumprod += counts[i][0]*counts[i][1];
			div1 += Math.pow(counts[i][0], 2);
			div2 += Math.pow(counts[i][1], 2);
		}
		return (float) (sumprod / (Math.sqrt(div1) * Math.sqrt(div2)));
	}
}
