package propextraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import splitting.CodeTextSplitter;

public class ReadingFeatureExtractor extends FeatureExtractor {
	public static void main(String[] args) {
		try {
			File fragmentsRoot = new File("../splitsnippets");
			File outputFile = new File("reading-features.csv");
			FeatureExtractor extractor = new ReadingFeatureExtractor(fragmentsRoot);
			extractor.process(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private final File execDir = new File("C:/TUDelft/Thesis/Text-Statistics/src/DaveChild/TextStatistics");
	private final String COMMAND_BASE = "C:/TUDelft/Thesis/php/php.exe -r \""
		+ "include('Text.php');"
		+ "include('Syllables.php');"
		+ "include('Maths.php');"
		+ "include('TextStatistics.php');"
		+ "include('Pluralise.php');";

	private final Map<String, Integer> wordCounts;
	private int totalCount;
	public ReadingFeatureExtractor(File fragmentsRoot) throws IOException {
		super(fragmentsRoot);
		this.wordCounts = new HashMap<String, Integer>();
		initWordCounts();
	}

	private void initWordCounts() throws IOException {
		this.totalCount = 0;
		for (Integer id : this.ids) {
			String readLines = readLines(getTextFile(id));
			String[] split = readLines.split("\\s+");
			for (String word : split) {
				Integer count = wordCounts.get(word);
				if (count == null) count = 0;
				this.wordCounts.put(word, count + 1);
				this.totalCount++;
			}
		}
	}

	@Override
	protected void process(Integer id, FileWriter outputWriter) throws IOException {
		if (Math.random() < 0.025)
			System.out.println(id);
		String content = readLines(getTextFile(id));
		content = content.replaceAll("<[^>]*>", "");
		try {
			double avgEntropy = avgEntropy(content, this.wordCounts, this.totalCount);
			double automatedRI = automatedRI(content);
			double colemanLiau = colemanLiau(content);
			double fleschGrade = fleschGrade(content);
			double fleschReading = fleschReading(content);
			double gunningFog = gunningFog(content);
			double locPerc = locPerc(getCodeFiles(id), content);
			double metricEntropy = metricEntropy(content);
			int sentencesCount = (int) sentencesCount(content);
			double smogGrade = SMOGGrade(content);
			int wordsCount = (int) wordsCount(content);
			outputWriter.append(id + "," + avgEntropy + "," + automatedRI + "," + colemanLiau + "," + 
					fleschGrade + "," + fleschReading + "," + gunningFog + "," + locPerc + "," +
					metricEntropy + "," + sentencesCount + "," + smogGrade + "," + wordsCount + "\n");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Error | Exception e) {
			System.err.println(id + "\t" + e);
		}
		outputWriter.flush();
	}

	private double avgEntropy(String content, Map<String, Integer> wordCounts, int totalCount) throws IOException, InterruptedException {
		String[] split = content.split("\\s+");
		double entropySum = 0.0;
		for (String word : split) {
			Integer count = wordCounts.get(word);
			double prob = (double) count / (double) totalCount;
			double entropy = -Math.log(prob)/Math.log(2);
			entropySum += entropy;
		}
		entropySum /= split.length;
		return entropySum;
	}
	
	private double colemanLiau(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->coleman_liau_index('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double automatedRI(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->automated_readability_index('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double fleschGrade(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->flesch_kincaid_grade_level('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double fleschReading(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->flesch_kincaid_reading_ease('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double gunningFog(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->gunning_fog_score('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double locPerc(List<File> codeFiles, String content) throws IOException {
		double codeLineCount = 0;
		for (int i = 0; i < codeFiles.size(); i++) {
			File codeFileI = codeFiles.get(i);
			if (!codeFileI.exists()) break;
			String[] lines = CodeTextSplitter.read(codeFileI).split("\n");
			codeLineCount += lines.length;
		}
		return codeLineCount / (content.split("\n").length + codeLineCount);
	}

	private double metricEntropy(String content) throws IOException, InterruptedException {
		char[] chars = content.toCharArray();
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		// count the occurrences of each value
		for (Character c : chars) {
			if (!map.containsKey(c)) {
				map.put(c, 0);
			}
			map.put(c, map.get(c) + 1);
		}
		// calculate the entropy
		Double result = 0.0;
		for (Character sequence : map.keySet()) {
			Double frequency = (double) map.get(sequence) / content.length();
			result -= frequency * (Math.log(frequency) / Math.log(2));
		}
		return result;
	}

	private double sentencesCount(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\Text;"
				+ "echo $x->sentenceCount('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double SMOGGrade(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\TextStatistics;"
				+ "echo $x->smog_index('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double wordsCount(String content) throws IOException, InterruptedException {
		String escaped = escape(content);
		String command = COMMAND_BASE
				+ "$x = new DaveChild\\TextStatistics\\Text;"
				+ "echo $x->wordCount('" + escaped + "');"
				+ "\"";
		return runCommand(command);
	}

	private double runCommand(String command) throws IOException,
			InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process pr = null;
		try {
			pr = rt.exec(command, null, execDir);
			String x = processResult(pr);
			return x == null ? 0.0 : Double.parseDouble(x);
		}
		finally {
			if (pr != null) {
				pr.destroy();
			}
		}
	}
	
	private String processResult(Process pr) throws IOException {
		StringBuilder output = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = null;
		while ((line = in.readLine()) != null) {
			output.append(line);
			output.append("\r\n");
		}
		in.close();
		return output.toString();
	}

	private String escape(String content) {
		String escaped = content.replaceAll("\\\\", "\\\\\\\\");
		escaped = escaped.replaceAll("\"", "\\\\\"");
		escaped = escaped.replaceAll("\'", "\\\\\'");
		return escaped;
	}
}
