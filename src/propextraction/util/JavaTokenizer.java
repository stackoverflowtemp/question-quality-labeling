package propextraction.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaTokenizer {
	public static Stream<String> tokenizeCode(String line) {
		return Arrays.stream(line.split("\\s+"))
				.map(JavaTokenizer::splitPunctuation)
				.flatMap(x -> x)
				.filter(x -> !x.isEmpty());
	}
	
	public static Stream<String> splitPunctuation(String in) {
		// Filter all strings first; replace these after tokenizing
		Pattern stringFilter = Pattern.compile("\"[^\"]*\"");
		Matcher m = stringFilter.matcher(in);
		String rep = in;
		Map<String, String> replacements = new HashMap<String, String>();
		int idx = 0;
		while (m.find()) {
			String placeholder = "STRPLCHLDR" + idx++;
			replacements.put(placeholder, m.group());
			rep = rep.replace(m.group(), placeholder);
		}
		in = rep;
		// Split on punctuation and whitespace, but first establish what operators not to break
		String[] delimeters = new String[] { ">>>=", ">>>", "<<<=", "<<<", ">>=", ">>", "<<=", "<<", ">=", "<=",
				"++", "--", "+=", "-=", "*=", "/=", "%=", "&=", "^=", "!=", "&&", "||", "==", "//", "/**", "/*", "*/" };
		String delimiterString = Arrays.stream(delimeters).map(Pattern::quote).collect(Collectors.joining("|"));
		String delimiterRegex = "((?<=(" + delimiterString + "))|(?=(" + delimiterString + ")))";
		String punctuation = "((?<=\\p{Punct})|(?=\\p{Punct}))";
		return Arrays.stream(in.split(delimiterRegex))
			.map(x -> x.matches(delimiterString) ? new String[]{ x } : x.split(punctuation))
			.flatMap(x -> Arrays.stream(x))
			.map(x -> replacements.containsKey(x) ? replacements.get(x) : x);
	}
}
