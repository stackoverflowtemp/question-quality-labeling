package propextraction.util;

public class Pair<T, V> {
	public final T left;
	public final V right;

	public Pair(T t, V v) {
		this.left = t;
		this.right = v;
	}
}