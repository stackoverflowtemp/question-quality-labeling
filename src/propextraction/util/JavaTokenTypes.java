package propextraction.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JavaTokenTypes {

	public static boolean isID(String token) {
		if (KEYWORDS.contains(token) || LITERALS.contains(token)) return false;
		return token.matches("\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*");
	}
	
	public static boolean isKeyword(String token) {
		return KEYWORDS.contains(token);
	}
	
	public static boolean isLiteral(String token) {
		return KEYWORDS.contains(token);
	}
	
	public static boolean isArithmeticOperator(String token) {
		return ARITHMETIC_OPERATORS.contains(token);
	}
	
	public static boolean isConditionalOperator(String token) {
		return CONDITIONAL_OPERATORS.contains(token);
	}
	
	private static final String[] aritmeticOps = new String[] {
		">>>=" , 
		">>>" , 
		"<<<=" , 
		"<<<" , 
		">>=" , 
		">>" , 
		"<<=" , 
		"<<" , 
		"++" ,
		"--" ,
		"+=" , 
		"-=" , 
		"*=" , 
		"/=" , 
		"%=" , 
		"&=" ,
		"|=" , 
		"^=" ,
		"+" ,
		"-" ,
		"*" ,
		"/" ,
		"%" ,
		"&" ,
		"|" ,
		"^" ,
	};
	private static Set<String> ARITHMETIC_OPERATORS = new HashSet<String>(Arrays.asList(aritmeticOps));
	
	private static final String[] conditionalOps = new String[] {
		">" ,
		"<" ,
		">=" , 
		"<=" , 
		"&&" ,
		"||" ,
		"==" ,
		"!" ,
		"!="
	};
	private static Set<String> CONDITIONAL_OPERATORS = new HashSet<String>(Arrays.asList(conditionalOps));
	
	private static final String[] LITS = new String[] {
		"null",
		"true",
		"false"
	};
	private static Set<String> LITERALS = new HashSet<String>(Arrays.asList(LITS));
	
	private static final String[] KEYS = new String[] {
		"abstract" ,
		"continue" ,
		"for" ,
		"new" ,
		"switch" ,
		"assert" ,
		"default" ,
		"goto" ,
		"package" ,
		"synchronized" ,
		"boolean" ,
		"do" ,
		"if" ,
		"private" ,
		"this" ,
		"break" ,
		"double" ,
		"implements" ,
		"protected" ,
		"throw" ,
		"byte" ,
		"else" ,
		"import" ,
		"public" ,
		"throws" ,
		"case" ,
		"enum" ,
		"instanceof" ,
		"return" ,
		"transient" ,
		"catch" ,
		"extends" ,
		"int" ,
		"short" ,
		"try" ,
		"char" ,
		"final" ,
		"interface" ,
		"static" ,
		"void" ,
		"class" ,
		"finally" ,
		"long" ,
		"strictfp" ,
		"volatile" ,
		"const" ,
		"float" ,
		"native" ,
		"super" ,
		"while"
	};
	private static Set<String> KEYWORDS = new HashSet<String>(Arrays.asList(KEYS));
}
