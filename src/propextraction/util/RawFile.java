package propextraction.util;

public class RawFile {
	private String title;
	private String tags;
	private String content;

	public RawFile(String title, String tags, String content) {
		this.setTitle(title);
		this.setTags(tags);
		this.setContent(content);
	}
	
	public RawFile() {
		this.setTitle(null);
		this.setTags(null);
		this.setContent(null);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
}
