package propextraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import propextraction.util.RawFile;

public abstract class FeatureExtractor {
	
	protected final File fragmentsRoot;
	protected final List<Integer> ids;
	
	public FeatureExtractor(File fragmentsRoot) {
		this.fragmentsRoot = fragmentsRoot;
		List<Integer> ids = new ArrayList<Integer>();
		for (String child : fragmentsRoot.list()) {
			if (!child.endsWith("-raw")) continue;
			String id = child.substring(0, child.indexOf("-raw"));
			ids.add(Integer.parseInt(id));
		}
		Collections.sort(ids);
		this.ids = ids;
	}
	
	protected final void process(File outputFile) throws IOException {
		FileWriter outputWriter = new FileWriter(outputFile);
		for (Integer id : this.ids) {
			process(id, outputWriter);
		}
		outputWriter.close();
	}
	
	protected abstract void process(Integer id, FileWriter outputWriter) throws IOException;
	
	protected final List<String> getIDs(File statsFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(statsFile));
		String line = br.readLine();
		List<String> ids = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			String[] split = line.split(",");
			if (split.length == 0)
				continue;
			String id = split[0];
			ids.add(id);
		}
		br.close();
		return ids;
	}

	protected final File getRawFile(Integer id) {
		return new File(this.fragmentsRoot, id + "-raw");
	}

	protected final File getTextFile(Integer id) {
		return new File(this.fragmentsRoot, id + "-text");
	}

	protected final List<File> getCodeFiles(Integer id) {
		List<File> codeFiles = new ArrayList<File>();
		for (int i = 0; ; i++) {
			File codeFileI = new File(this.fragmentsRoot, id + "-code-" + i);
			if (!codeFileI.exists()) break;
			codeFiles.add(codeFileI);
		}
		return codeFiles;
	}
	
	protected final File getPOSFile(Integer id) {
		return new File(new File(this.fragmentsRoot, "pos"), id + ".txt-pos");
	}

	/**
	 * @Return separated out: the title and tags line (as a pair) followed by the content
	 */
	protected final RawFile readRaw(File fragmentFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fragmentFile));
		String line = null;
		StringBuilder content = new StringBuilder();
		RawFile rawFile = new RawFile();
		int i = 0;
		while ((line = br.readLine()) != null) {
			if (i == 0) {
				rawFile.setTitle(line);
			}
			else if (i == 1) {
				rawFile.setTags(line);
			}
			else {
				content.append(line);
				content.append("\n");
			}
			i++;
		}
		br.close();
		rawFile.setContent(content.toString());
		return rawFile;
	}

	protected final String readLines(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder content = new StringBuilder();
		while ((line = br.readLine()) != null) {
			content.append(line);
			content.append("\n");
		}
		br.close();
		return content.toString();
	}
}
