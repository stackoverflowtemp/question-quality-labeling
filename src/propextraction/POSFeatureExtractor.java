package propextraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;

public class POSFeatureExtractor extends FeatureExtractor {
	
	private static final int THRESHOLD = 895;
	private final static int LEVEL = 5;
	public static void main(String[] args) {
		try {
			File fragmentsRoot = new File("../goodbadsnippets");
			File outputFile = new File("pos-features" + LEVEL);
			FeatureExtractor extractor = new POSFeatureExtractor(fragmentsRoot);
			extractor.process(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Map<String, Integer> POSCounts = new HashMap<String, Integer>();
	private Set<String> allowedPOS;
	
	public POSFeatureExtractor(File fragmentsRoot) throws IOException {
		super(fragmentsRoot);
		readAllowed();
	}

	private void readAllowed() throws IOException {
		this.allowedPOS = new HashSet<String>();
		Files.lines(new File("pos-allowed").toPath()).forEach(x -> this.allowedPOS.add(x));
	}

	@Override
	protected void process(Integer id, FileWriter outputWriter)
			throws IOException {
		if (id == this.ids.get(0)) {
			getCounts(outputWriter);
		}
		if (Math.random() < 0.01) System.out.println(id);
		String readLines;
		try {
			readLines = readLines(getPOSFile(id));
		} catch (FileNotFoundException e) {
			appendEmpty(outputWriter);
			return;
		}
		processFile(outputWriter, readLines);
	}

	private void processFile(FileWriter outputWriter, String readLines)
			throws IOException {
		Map<String, Integer> fileCounts = getPOSs(readLines);
		StringBuilder line = new StringBuilder();
		for (Entry<String, Integer> entry : this.POSCounts.entrySet()) {
			if (entry.getValue() < THRESHOLD) continue;
			String POS = entry.getKey();
			if (fileCounts.containsKey(POS)) {
				line.append("1,");
			}
			else {
				line.append("0,");
			}
		}
		outputWriter.append(line.substring(0, line.length() - 1) + "\n");
	}

	private void getCounts(FileWriter outputWriter) throws IOException {
		File posFile = new File("poscountsINC3" + LEVEL + ".csv");
		if (posFile.exists()) {
			readPOSCounts(posFile);
		} else {
			initPosCounts(posFile);
		}
		for (Iterator<Entry<String, Integer>> iterator = this.POSCounts.entrySet()
				.iterator(); iterator.hasNext();) {
			Entry<String, Integer> entry = iterator.next();
			if (entry.getValue() < THRESHOLD) {
				iterator.remove();
			}
		}
		writeHeader(outputWriter);
	}

	private void readPOSCounts(File posFile) throws NumberFormatException, IOException {
		Files.lines(posFile.toPath()).filter(x -> !x.isEmpty()).forEach(x -> {
			int commaIndex = x.lastIndexOf(",");
			String word = x.substring(0, commaIndex);
			Integer count = Integer.parseInt(x.substring(x.lastIndexOf(",") + 1));
			this.POSCounts.put(StringEscapeUtils.escapeJava(word), count);
		});		
	}

	private void initPosCounts(File posFile) throws IOException {
		for (Integer id : this.ids) {
			if (Math.random() < 0.01) System.out.println("S: " + id);
			try {
				String readLines = readLines(getPOSFile(id));
				Map<String, Integer> fileCounts = getPOSs(readLines);
				for (Entry<String, Integer> entry : fileCounts.entrySet()) {
					String word = entry.getKey();
					Integer count = entry.getValue();
					Integer prev = this.POSCounts.get(word);
					if (prev == null) prev = 0;
					this.POSCounts.put(word, prev + count);
				}
			} catch (FileNotFoundException e) {
				continue;
			}
			
		}
		FileWriter fw = new FileWriter(posFile);
		for (Entry<String, Integer> entry : this.POSCounts.entrySet()) {
			fw.append(entry.getKey() + "," + entry.getValue() + "\n");
		}
		fw.close();
	}

	private void writeHeader(FileWriter outputWriter) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Integer> entry : this.POSCounts.entrySet()) {
			if (entry.getValue() < THRESHOLD) continue;
			String word = entry.getKey();
			sb.append(word + ",");
		}
		outputWriter.append(sb.substring(0, sb.length() - 1) + "\n");
	}

	private Map<String, Integer> getPOSs(String readLines) {
		Map<String, Integer> POSs = new HashMap<String, Integer>();
		List<String> context = new ArrayList<String>();
		for (String word : readLines.split("\\s+")) {
			String pos = getPos(word);
			if (pos == null) continue;
			if (context.size() == LEVEL) {
				String concat = context.stream().collect(Collectors.joining("-"));
				Integer prev = POSs.get(concat);
				if (prev == null) prev = 0;
				POSs.put(concat, prev + 1);
				for (int i = 0; i < context.size() - 1; i++) {
					context.set(i, context.get(i + 1));
				}
				context.set(context.size() - 1, pos);
			}
			else {
				context.add(pos);
			}
		}
		return POSs;
	}

	private String getPos(String word) {
		if (word.matches("<(/)?javacode>") || word.matches("<(/)?error>")) {
			return word;
		}
		String[] split = word.split("/");
		if (split.length == 2 && !split[0].isEmpty()) {
			if (this.allowedPOS.contains(split[1])) {
				return split[1];
			}
		}
		return null;
	}

	private void appendEmpty(FileWriter outputWriter) throws IOException {
		StringBuilder line = new StringBuilder();
		for (int i = 0; i < this.POSCounts.size(); i++) {
			line.append("0,");
		}
		outputWriter.append(line.substring(0, line.length() - 1) + "\n");
	}
}
