package propextraction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringEscapeUtils;

public class TermFeatureExtractor extends FeatureExtractor {

	private final int COUNT_CUTOFF = 44;

	public static void main(String[] args) {
		try {
			File fragmentsRoot = new File("../splitsnippets");
			File outputFile = new File("term-features2");
			FeatureExtractor extractor = new TermFeatureExtractor(fragmentsRoot);
			extractor.process(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private final Map<String, Integer> wordCounts;
	
	public TermFeatureExtractor(File fragmentsRoot) throws IOException {
		super(fragmentsRoot);
		this.wordCounts = new HashMap<String, Integer>();
	}
	
	@Override
	protected void process(Integer id, FileWriter outputWriter)
			throws IOException {
		if (Math.random() < 0.01) {
			System.out.println(id);
		}
		// At the first ID: get word counts and populate header
		if (id == this.ids.get(0)) {
			File wordCountsFile = new File("wordcounts.csv");
			if (wordCountsFile.exists()) {
				readWordCounts(wordCountsFile);
			} else {
				countWords(wordCountsFile);
			}
			writeHeader(outputWriter);
		}
		processsID(id, outputWriter);
	}

	private void readWordCounts(File wordCountsFile) throws IOException {
		Files.lines(wordCountsFile.toPath()).filter(x -> !x.isEmpty()).forEach(x -> {
			int commaIndex = x.lastIndexOf(",");
			String word = x.substring(0, commaIndex);
			Integer count = Integer.parseInt(x.substring(x.lastIndexOf(",") + 1));
			this.wordCounts.put(StringEscapeUtils.escapeJava(word), count);
		});
	}

	private void countWords(File wordCountsFile) throws IOException {
		for (Integer i : this.ids) {
			if (Math.random() < 0.01)
				System.out.println("S: " + i);
			String body = readLines(getTextFile(i));
			List<String> split = getWords(body);
			for (String word : split) {
				if (!wordValid(word)) continue;
				Integer count = this.wordCounts.get(word);
				if (count == null) count = 0;
				this.wordCounts.put(word, count + 1);
			}
			String title = readRaw(getRawFile(i)).getTitle();
			split = getWords(title);
			for (String word : split) {
				if (!wordValid(word)) continue;
				Integer count = this.wordCounts.get(word);
				if (count == null) count = 0;
				this.wordCounts.put(word, count + 1);
			}
		}
		FileWriter fw = new FileWriter(wordCountsFile);
		for (Entry<String, Integer> entry : this.wordCounts.entrySet()) {
			fw.append(entry.getKey() + "," + entry.getValue() + "\n");
		}
		fw.close();
	}

	private void writeHeader(FileWriter outputWriter) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Integer> entry : this.wordCounts.entrySet()) {
			if (!valid(entry)) continue;
			String word = entry.getKey();
			sb.append(word + ",");
		}
		outputWriter.append(sb.substring(0, sb.length() - 1) + "\n");
	}

	private void processsID(Integer id, FileWriter outputWriter)
			throws IOException {
		StringBuilder sb = new StringBuilder();
		Set<String> terms = getTerms(id);
		for (Entry<String, Integer> entry : this.wordCounts.entrySet()) {
			String word = entry.getKey();
			if (!valid(entry)) continue;
			if (terms.contains(word)) {
				sb.append("1,");
			}
			else {
				sb.append("0,");
			}
		}
		outputWriter.append(sb.substring(0, sb.length() - 1) + "\n");
	}

	private boolean valid(Entry<String, Integer> entry) {
		String word = entry.getKey();
		Integer count = entry.getValue();
		return wordValid(word) && countValid(count);
	}

	private boolean countValid(Integer count) {
		return count > this.COUNT_CUTOFF;
	}

	private boolean wordValid(String word) {
		return word.matches("(I|a)") || word.length() > 1;
	}

	private Set<String> getTerms(Integer id) throws IOException {
		String title = readRaw(getRawFile(id)).getTitle();
		String content = readLines(getTextFile(id));
		Set<String> words = new HashSet<String>();
		for (String word : getWords(title)) {
			words.add(word);
		}
		for (String word : getWords(content)) {
			words.add(word);
		}
		return words;
	}

	private List<String> getWords(String body) {
		return Arrays.stream(body.split("[\\s\\p{Punct}]+"))
				.filter(x -> !x.isEmpty())
				.filter(x -> x.matches("[a-zA-Z]+"))
				.map(x -> StringEscapeUtils.escapeJava(x))
				.collect(Collectors.toList());
	}
}
