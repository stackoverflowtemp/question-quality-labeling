package propextraction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import propextraction.util.JavaTokenTypes;
import propextraction.util.JavaTokenizer;
import propextraction.util.Pair;
import splitting.CodeTextSplitter;

public class CodeFeatureExtractor extends FeatureExtractor {
	public static void main(String[] args) {
		try {
			File fragmentsRoot = new File("../splitsnippets");
			File outputFile = new File("code-features.csv");
			FeatureExtractor extractor = new CodeFeatureExtractor(fragmentsRoot);
			extractor.process(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public CodeFeatureExtractor(File fragmentsRoot) {
		super(fragmentsRoot);
	}

	@Override
	protected void process(Integer id, FileWriter outputWriter) throws IOException {
		if (Math.random() < 0.025) System.out.println(id);
		List<File> codeFiles = getCodeFiles(id);
		if (codeFiles.isEmpty()) {
			appendEmpty(id, outputWriter);
			return;
		}
		List<String> contents = getContents(codeFiles);
		List<String> codeLines = getCodeLines(contents);
		if (codeLines.isEmpty()) {
			appendEmpty(id, outputWriter);
			return;
		}
		List<List<String>> tokens = getTokens(contents);
		// Extract features
		Pair<Float, Integer> lineLengths = lineLengths(codeLines);
		Pair<Float, Integer> idCounts = idCounts(tokens);
		Pair<Float, Integer> idLength = idLengths(tokens);
		Pair<Float, Integer> indentationLengths = indentationLengths(contents);
		Pair<Float, Integer> keywordCounts = keywordCounts(tokens);
		Pair<Float, Integer> numberCounts = numberCounts(tokens);
		Float commentCount = commentCount(contents);
		Float periodCount = (float) periodCount(tokens) / codeFiles.size();
		Float commaCount = (float) commaCount(tokens) / codeFiles.size();
		Float spaceCount = (float) spaceCount(contents) / codeFiles.size();
		Float parenthesisCount = (float) parenthesisCount(tokens) / codeFiles.size();
		Float arithmOpsCount = (float) arithmOpsCount(tokens) / codeFiles.size();
		Float compOpsCount = (float) compOpsCount(tokens) / codeFiles.size();
		Float assignmentsCount = (float) assignmentsCount(tokens) / codeFiles.size();
		Float branchesCount = (float) branchesCount(tokens) / codeFiles.size();
		Float loopsCount = (float) loopsCount(tokens) / codeFiles.size();
		Float blankLinesCount = (float) blankLinesCount(contents) / codeFiles.size();
		int singleCharMax = singleChars(tokens);
		int singleIDMax = singleIDs(tokens);
		outputWriter.append(id + "," + lineLengths.left + "," + lineLengths.right + "," +
				idCounts.left + "," + idCounts.right + "," + idLength.left + "," + idLength.right + "," +
				indentationLengths.left + "," + indentationLengths.right + "," + 
				keywordCounts.left + "," + keywordCounts.right + "," + numberCounts.left + "," + numberCounts.right + "," +
				commentCount + "," + periodCount + "," + commaCount + "," + spaceCount + "," + parenthesisCount + "," +
				arithmOpsCount + "," + compOpsCount + "," + assignmentsCount + "," + branchesCount + "," +
				loopsCount + "," + blankLinesCount + "," + singleCharMax + "," + singleIDMax + "\n");
		outputWriter.flush();
	}

	private static Pair<Float, Integer> lineLengths(List<String> codeLines) {
		int[] maxLine = {0};
		double average = codeLines.stream()
			.mapToDouble(x -> x.length())
			.peek(x -> {if (x > maxLine[0]) maxLine[0] = (int) x; })
			.average().orElse(0.0);
		return new Pair<Float, Integer>((float) average, maxLine[0]);
	}
	
	private static Pair<Float, Integer> idCounts(List<List<String>> tokens) {
		int max = 0;
		float avg = 0.0f;
		for (List<String> fragTokens : tokens) {
			int count = 0;
			for (String token : fragTokens) {
				if (JavaTokenTypes.isID(token)) {
					count++;
				}
			}
			avg += count;
			max = Math.max(max, count);
		}
		avg /= tokens.size();
		return new Pair<Float, Integer>(avg, max);
	}
	
	private static Pair<Float, Integer> idLengths(List<List<String>> tokens) {
		int max = 0;
		float avg = 0.0f;
		for (List<String> fragTokens : tokens) {
			int count = 0;
			int lengthSum = 0;
			for (String token : fragTokens) {
				if (JavaTokenTypes.isID(token)) {
					count++;
					lengthSum += token.length();
					max = Math.max(max, token.length());
				}
			}
			if (count == 0) continue;
			avg += (float) lengthSum / (float) count;
		}
		avg /= tokens.size();
		return new Pair<Float, Integer>(avg, max);
	}

	private static Pair<Float, Integer> indentationLengths(List<String> contents) {
		Pattern p = Pattern.compile("\\s+");
		int max = 0;
		float avg = 0.0f;
		for (String content : contents) {
			int indentationSum = 0;
			String[] lines = content.split("\n");
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i];
				Matcher m = p.matcher(line);
				if (m.find() && m.start() == 0) {
					indentationSum++;
				}
			}
			max = Math.max(max, indentationSum);
			avg += (float) indentationSum / lines.length;
		}
		avg /= contents.size();
		return new Pair<Float, Integer>(avg, max);
	}
	
	private static Pair<Float, Integer> keywordCounts(List<List<String>> tokens) {
		int max = 0;
		float avg = 0.0f;
		for (List<String> fragTokens : tokens) {
			int count = 0;
			for (String token : fragTokens) {
				if (JavaTokenTypes.isKeyword(token)) {
					count++;
				}
			}
			avg += count;
			max = Math.max(max, count);
		}
		avg /= tokens.size();
		return new Pair<Float, Integer>(avg, max);
	}
	
	private static Pair<Float, Integer> numberCounts(List<List<String>> tokens) {
		int max = 0;
		float avg = 0.0f;
		for (List<String> fragTokens : tokens) {
			int count = 0;
			for (String token : fragTokens) {
				try {
					Integer.parseInt(token);
					count++;
					continue;
				} catch (NumberFormatException e) {
				}
				try {
					Long.parseLong(token);
					count++;
					continue;
				} catch (NumberFormatException e) {
				}
				try {
					Float.parseFloat(token);
					count++;
					continue;
				} catch (NumberFormatException e) {
				}
				try {
					Double.parseDouble(token);
					count++;
					continue;
				} catch (NumberFormatException e) {
				}
			}
			avg += count;
			max = Math.max(max, count);
		}
		avg /= tokens.size();
		return new Pair<Float, Integer>(avg, max);
	}
	
	private static Float commentCount(List<String> contents) {
		float commentCount = 0.0f;
		for (String content : contents) {
			Pattern lineComments = Pattern.compile("//[^\n]+\n");
			Matcher m = lineComments.matcher(content);
			while (m.find()) {
				commentCount++;
			}
			String start = Pattern.quote("/**") + "?";
			String end = Pattern.quote("*/");
			Pattern blockComments = Pattern.compile(start + "[^\\*]+?" + end, Pattern.DOTALL);
			m = blockComments.matcher(content);
			while (m.find()) {
				commentCount++;
			}
		}
		return commentCount / contents.size();
	}

	private static long periodCount(List<List<String>> tokens) {
		return tokens.stream().map(x -> x.stream()).flatMap(x -> x).filter(x -> x.equals(".")).count();
	}
	
	private static long commaCount(List<List<String>> tokens) {
		return tokens.stream().map(x -> x.stream()).flatMap(x -> x).filter(x -> x.equals(",")).count();
	}

	private static int spaceCount(List<String> fileContents) {
		int spaces = 0;
		for (String file : fileContents) {
			for (char c : file.toCharArray()) {
				if (Character.isSpaceChar(c)) spaces++;
			}
		}
		return spaces;
	}

	private static long parenthesisCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> x.matches("[\\(\\)\\]\\[\\}\\{]"))
				.count();
	}

	private static long arithmOpsCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> JavaTokenTypes.isArithmeticOperator(x))
				.count();
	}

	private static long compOpsCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> JavaTokenTypes.isConditionalOperator(x))
				.count();

	}

	private static long assignmentsCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> x.equals("="))
				.count();
	}

	private static long branchesCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> x.equals("if"))
				.count();
	}

	private static long loopsCount(List<List<String>> tokens) {
		return tokens.stream()
				.map(x -> x.stream())
				.flatMap(x -> x)
				.filter(x -> x.matches("(for|while)"))
				.count();
	}

	private static long blankLinesCount(List<String> contents) {
		return contents.stream()
				.map(x -> Arrays.stream(x.split("\n")))
				.flatMap(x -> x)
				.filter(String::isEmpty)
				.count();
	}

	private static int singleChars(List<List<String>> tokens) {
		Map<String, Integer> counts = new HashMap<String, Integer>();
		tokens.stream().forEach(x -> x.stream().forEach(y -> {
			if (y.length() == 1 && !JavaTokenTypes.isID(y)) {
				Integer count = counts.get(y);
				if (count == null) count = 0;
				counts.put(y, count + 1);
			}
		}));
		return counts.values().stream().mapToInt(x -> x).max().orElse(0);
	}
	
	private static int singleIDs(List<List<String>> tokens) {
		Map<String, Integer> counts = new HashMap<String, Integer>();
		tokens.stream().forEach(x -> x.stream().forEach(y -> {
			if (JavaTokenTypes.isID(y)) {
				Integer count = counts.get(y);
				if (count == null) count = 0;
				counts.put(y, count + 1);
			}
		}));
		return counts.values().stream().mapToInt(x -> x).max().orElse(0);
	}

	private static String emptyLine = null;
	private static void appendEmpty(Integer id, FileWriter outputWriter) throws IOException {
		if (emptyLine == null) {
			emptyLine = "";
			for (int i = 0; i < 25; i++) emptyLine += ",0";
			emptyLine += "\n";
		}
		outputWriter.append(id + emptyLine);
	}

	private List<List<String>> getTokens(List<String> contents) {
		List<List<String>> tokens = contents.stream()
			.map(JavaTokenizer::tokenizeCode)
			.map(x -> x.collect(Collectors.toList()))
			.collect(Collectors.toList());
		return tokens;
	}

	private List<String> getCodeLines(List<String> contents) {
		List<String> codeLines = contents.stream()
			.map(x -> Arrays.stream(x.split("\n")))
			.flatMap(x -> x)
			.filter(x -> !x.isEmpty())
			.collect(Collectors.toList());
		return codeLines;
	}

	private List<String> getContents(List<File> codeFiles) {
		List<String> contents = codeFiles.stream().map(x -> {
				try {
					return CodeTextSplitter.read(x);
				} catch (IOException e) {
					return "";
				}
			})
			.filter(x -> !x.trim().isEmpty())
			.collect(Collectors.toList());
		return contents;
	}
}
